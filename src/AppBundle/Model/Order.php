<?php
/**
 * Created by PhpStorm.
 * User: vagrant
 * Date: 4/16/16
 * Time: 1:05 AM
 */

namespace AppBundle\Model;


class Order
{
    /** @var int $jobId */
    private $jobId;

    /** @var int $jobId */
    private $streamId;

    /** @var int $header */
    private $header;

    /** @var array $lines */
    private $lines;

    /**
     * Order constructor.
     * 
     * @param int $jobId
     * @param int $streamId
     * @param int $header
     * @param array $lines
     */
    public function __construct($jobId, $streamId, $header, $lines = [])
    {
        $this->jobId = $jobId;
        $this->streamId = $streamId;
        $this->header = $header;
        $this->lines = $lines;
    }

    /**
     * Adds new line to the array of lines.
     *
     * @param string $unitName
     * @param int $quantity
     */
    public function addLine($unitName, $quantity)
    {
        $this->lines[$unitName] = $quantity;
    }

    /**
     * Checks if current order is valid.
     *
     * @return bool
     */
    public function isValid()
    {
        $isValid = false;
        if ($this->jobId > 0 &&
            $this->streamId > 0 &&
            $this->header > 0 &&
            count($this->lines)
        ) {
            foreach ($this->lines as $product => $quantity) {
                if (!empty($product) && !empty($quantity) &
                    $quantity > 0 && $quantity < 6
                ) {
                    $isValid = true;
                    break;
                }
            }
        }

        return $isValid;
    }

    public function getJobId()
    {
        return $this->jobId;
    }

    public function getStreamId()
    {
        return $this->streamId;
    }

    public function getHeader()
    {
        return $this->header;
    }

    /**
     * Returns array of lines.
     * If $filter = true then only lines with Quantity > 0 are returned.
     *
     * @param bool $filter
     *
     * @return array
     */
    public function getLines($filter = false)
    {
        return $filter ?
            array_filter(
                $this->lines,
                function ($quantity) {
                    return $quantity > 0;
                }
            ) :
            $this->lines;
    }

    /**
     * Stringifies Order object.
     *
     * @return string
     */
    public function __toString()
    {
        return \json_encode($this->toArray(), JSON_UNESCAPED_UNICODE);
    }

    /**
     * Serializes object.
     * By default uses extra serialization to base64 to workaround issue
     * with NSQ unserialization.
     *
     * @param bool $toBase64
     *
     * @return string
     */
    public function serialize($toBase64 = true)
    {
        return $toBase64 ? base64_encode((string)$this) : (string)$this;
    }

    /**
     * Creates new order from serialized string.
     * By default assumes string, being previously extra serialized to base64
     * to workaround issue with NSQ unserialization.
     *
     * @param string $orderString
     * @param bool $fromBase64
     *
     * @return Order|null
     */
    public static function createFromString($orderString, $fromBase64 = true)
    {
        $orderArray = json_decode(
            $fromBase64 ? base64_decode($orderString) : $orderString,
            true
        );
        return is_array($orderArray) ? self::fromArray($orderArray) : null;
    }

    /**
     * Creates new order from array.
     *
     * @param $orderArray
     *
     * @return Order|null
     */
    protected static function fromArray($orderArray)
    {
        $order = new self(
            $orderArray['job'],
            $orderArray['stream'],
            $orderArray['order']['header'],
            $orderArray['order']['lines']
        );

        return $order->isValid() ? $order : null;
    }

    /**
     * Converts order instance to array.
     * For $full=false the order is returned without jobId and streamId
     * to comply with examples in the task description.
     *
     * @param bool $full
     * @return array
     */
    protected function toArray($full = true)
    {
        return $full ? [
            'job' => $this->jobId,
            'stream' => $this->streamId,
            'order' => $this->toArray(false),
        ] : [
            'header' => $this->header,
            'lines' => $this->getLines(true),
        ];
    }
}