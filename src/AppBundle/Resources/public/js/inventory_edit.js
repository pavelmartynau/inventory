(function () {
    angular.module('app').controller('InventoryEditCtrl', function ($scope, $uibModal, $http, $log) {

        $scope.animationsEnabled = true;

        $scope.open = function () {

            $uibModal.open({
                animation: $scope.animationsEnabled,
                templateUrl: 'invEditContent.html',
                controller: ModalInstanceCtrl,
                resolve: {
                    items: function () {
                        return [];
                    }
                }
            });
        };

        $scope.runAllocateInventory = function () {
            $http({
                method: 'POST',
                url: 'api/jobs',
                data: $scope.inventory, //forms user object
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            })
            .success(function (data) {
                $scope.runJob(data.job);
            })
        };

        $scope.runJob = function (job_id) {
            var modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                templateUrl: 'jobProgress.html',
                controller: ModalInstanceCtrl,
                keyboard: false,
                backdrop: false,
                resolve: {
                    items: function () {
                        return [];
                    }

                }
            });

            modalInstance.opened.then(function() {
                alert(job_id);
            });
        };

        $scope.toggleAnimation = function () {
            $scope.animationsEnabled = !$scope.animationsEnabled;
        };

    });

    var ModalInstanceCtrl = function ($scope, $uibModalInstance, items, $http, $window) {

        $scope.items = items;
        $scope.selected = {
            item: $scope.items[0]
        };

        //$scope.ok = function () {
        //}
        $scope.inventory = {};
        $scope.submit = function () {
            $http({
                method: 'PUT',
                url: '/api/inventories',
                data: $scope.inventory,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            })
            .success(function (data) {
                $uibModalInstance.close();
                $window.location.reload();
            })
            .error(function (data) {
                $scope.error = true;
                $scope.error_text = data['error']
                $uibModalInstance.close();
            });

        };


        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    };

})();