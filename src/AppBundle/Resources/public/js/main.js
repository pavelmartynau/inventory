(function () {
    angular.module('app')
        .controller('JobLogController', JobLogController);

    function JobLogController($scope, $location, $http, config, ngTableParams) {
        this.$http = $http;
        this.config = config;

        $scope.showJobLog = false;
        // Default values, usually fetched from the url
        // to allow direct access to the filtered table
        var page = 1;
        var count = 10;

        // Setup and publish the table on the scope
        $scope.tableParams = new ngTableParams({page: page, count: count},
            {
                total: 0,
                getData: function ($defer, tableParams) {
                    this.fetchLoggedItems(this.createQuery(tableParams), tableParams, $defer);
                }.bind(this)
            }
        );
    }

    /**
     * Create the query object we need to send to our API endpoint
     * from the table params.
     */
    JobLogController.prototype.createQuery = function (tableParams) {
        var query = {
            page: tableParams.page(),
            limit: tableParams.count()
        };

        return query;
    };

    /**
     * Fetch the product list by sending the HTTP request to the products endpoint.
     */
    JobLogController.prototype.fetchLoggedItems = function (query, tableParams, $defer) {
        this.$http({
            url: this.config.baseUrl + '/joblog',
            method: 'GET',
            params: query
        }).then(
            // Success callback
            function (response) {
                var data = response.data;
                var products = data._embedded.items;

                // Set the total number of products
                tableParams.total(data.total);

                // Resolve the defer with the products array
                $defer.resolve(products);
            }
        );
    }

})();