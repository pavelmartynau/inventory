<?php
/**
 * Created by PhpStorm.
 * User: vagrant
 * Date: 4/16/16
 * Time: 2:23 AM
 */

namespace AppBundle\EventListener;

use Socloz\NsqBundle\Topic\Topic;

class OrderGeneratedEventListener
{
    /** @var Topic */
    private $topic;

    /**
     * OrderGeneratedEventListener constructor.
     *
     * @param Topic $topic
     */
    public function __construct(Topic $topic)
    {
        $this->topic = $topic;
    }

    /**
     * @param OrderGeneratedEvent $event
     * @throws \Exception
     */
    public function onOrderGenerated(OrderGeneratedEvent $event)
    {
        $this->topic->publish($event->getOrder()->serialize());
    }
}