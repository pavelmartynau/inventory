<?php
/**
 * Created by PhpStorm.
 * User: vagrant
 * Date: 4/15/16
 * Time: 11:54 PM
 */

namespace AppBundle\EventListener;

use Symfony\Component\EventDispatcher\Event;

use AppBundle\Model\Order;

class OrderGeneratedEvent extends Event
{
    const EVENT_NAME = 'order.generated';

    /* @var Order $order */
    protected $order;

    /**
     * OrderGeneratedEvent constructor.
     *
     * @param Order $order
     */
    public function __construct($order)
    {
        $this->order = $order;
    }

    /**
     * @return Order
     */
    public function getOrder()
    {
        return $this->order;
    }
}