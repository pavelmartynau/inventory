<?php
/**
 * Created by PhpStorm.
 * User: vagrant
 * Date: 4/16/16
 * Time: 4:23 AM
 */

namespace AppBundle\Service;

use AppBundle\Entity\Inventory;
use AppBundle\Entity\Job;
use AppBundle\Repository\JobRepository;
use AppBundle\Repository\JobLogRepository;
use AppBundle\EventListener\OrderGeneratedEvent;
use AppBundle\Model\Order;
use AppBundle\Repository\InventoryRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;


class InventoryAllocator
{
    /** @var ContainerInterface */
    private $container;

    /** @var EntityManager */
    private $em;

    /** @var InventoryRepository */
    private $inventoryRepository;

    /** @var Inventory[] $inventories */
    private $inventories;

    /** @var int */
    private $logToFile;

    /** @var JobLogRepository */
    private $jobLogRepository;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;

        $this->em = $this->container->get('doctrine')->getManager();
        $this->inventoryRepository = $this->container->get('app.repository.inventory');
        $this->inventories = $this->inventoryRepository->findAll();

        $this->logToFile = $this->container->getParameter('log_to_file');

        $this->jobLogRepository = $this->container->get('app.repository.job_log');

    }

    /**
     * @param Order $order
     *
     * @return bool
     */
    public function allocate(Order $order)
    {
        $jobId = $order->getJobId();

        if (!$this->isJobInProgress($jobId)) {
            return false;
        }

        $streamId = $order->getStreamId();
        $header = $order->getHeader();


        $allocation = [
            'job' => $jobId,
            'stream' => $streamId,
            'allocs' => [
                'header' => $header,
                'res' => [],
            ],
        ];

        $requested = [];
        $allocated = [];
        $backordered = [];

        $orderLines = $order->getLines();

        foreach ($this->inventories as &$inventory) {
            $unitName = $inventory->getName();

            $requested[$unitName] = 0;
            $allocated[$unitName] = 0;
            $backordered[$unitName] = 0;

            if (array_key_exists($unitName, $orderLines)) {
                $quantity = $orderLines[$unitName];
                $requested[$unitName] = $quantity;
                if ($quantity > 0) {
                    //$allocated = $this->allocateLine($jobId, $streamId, $header, $unitName, $quantity);
                    if ($this->inventoryRepository->allocateInventoryUnitQuantity($inventory->getId(), $quantity)) {
                        $allocated[$unitName] = $quantity;
                    } else {
                        $backordered[$unitName] = $quantity;
                    }
                }
            }
        }

        $allocation['allocs']['res'] = [
            'requested' => $requested,
            'allocated' => $allocated,
            'backordered' => $backordered,
        ];

        $this->logAllocation($allocation);

        return true;
    }

    /**
     * @param int $jobId
     * 
     * @return bool
     */
    protected function isJobInProgress($jobId)
    {
        /** @var JobRepository $jobRepository */
        $jobRepository = $this->container->get('app.repository.job');

        /** @var Job $job */
        $job = $jobRepository->find($jobId);

        return $job ? !$job->getComplete() : false;
    }

    /**
     * @param array $allocation
     */
    protected function logAllocation($allocation)
    {
        if ($this->logToFile) {
            $this->logAllocationToFile($allocation);
        } else {
            $this->logAllocationToDatabase($allocation);
        }
     }

    protected function logAllocationToFile($allocation)
    {
        $allocationString = $this->formatAllocationString($allocation);
        file_put_contents('/tmp/inventory'.$allocation['job'].'.log', $allocationString.PHP_EOL, FILE_APPEND);
    }

    protected function logAllocationToDatabase($allocation)
    {
        $this->jobLogRepository->addNewRow($allocation);
    }
    
    /**
     * @param array $allocation
     */
    protected function formatAllocationString($allocation)
    {
        return sprintf(
            "% 6d(%02d)\t%s",
            $allocation['allocs']['header'],
            $allocation['stream'],
            implode(
                '::',
                [
                    implode(',', $allocation['allocs']['res']['requested']),
                    implode(',', $allocation['allocs']['res']['allocated']),
                    implode(',', $allocation['allocs']['res']['backordered']),
                ]
            )
        );
    }
}