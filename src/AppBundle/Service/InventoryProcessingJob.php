<?php
/**
 * Created by PhpStorm.
 * User: vagrant
 * Date: 4/15/16
 * Time: 9:48 PM
 */

namespace AppBundle\Service;

use AppBundle\Entity\Job;
use AppBundle\Repository\InventoryRepository;
use AppBundle\Repository\JobRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Process\Process;

class InventoryProcessingJob
{

    const STREAM_COMMAND_RUN_TEMPLATE = 'php app/console inventory:stream --job=%d --stream=%d%s';
    //  >/tmp/stream_%02d.log 2>&1';

    /** @var ContainerInterface */
    private $container;

    /** @var EntityManager */
    private $em;

    /** @var InventoryRepository */
    private $inventoryRepository;

    /** @var  Job */
    private $job;

    /** @var int */
    private $totalItemsQuantity;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->em = $this->container->get('doctrine')->getManager();
        $this->inventoryRepository = $this->container->get('app.repository.inventory');

        $this->job = null;
        $this->totalItemsQuantity = null;
    }

    /**
     * Execute inventory processing job.
     *
     * @param  int $numStreams
     * @param callable $onProgress
     * @param callable $onComplete
     *
     * @return void
     */
    public function execute($numStreams, $onProgress, $onComplete)
    {
        $this->totalItemsQuantity = $this->countUnprocessedItems();

        $this->creatNewJob();
        if (!$this->job) {
            throw new \RuntimeException('Failed to create new inventory processing job');
        }

        /** @var Process[] $processes */
        $processes = $this->createWorkingProcesses($numStreams);
        $this->waitJobCompletion($processes, $onProgress);
        $this->onJobCompletion($onComplete);
    }

    /**
     *
     */
    protected function creatNewJob()
    {
        /** @var JobRepository $jobRepository */
        $jobRepository = $this->container->get('app.repository.job');
        $this->job = $jobRepository->createNewJob();
    }

    protected function countUnprocessedItems()
    {
        return $this->inventoryRepository->countUnallocatedInventoryUnits();
    }

    /**
     * Create requested quantity of inventory processing streams.
     *
     * @param $numStreams
     *
     * @return Process[]
     */
    protected function createWorkingProcesses($numStreams)
    {
        /** @var Process[] $processes */
        $processes = [];
        for ($stream = 1; $stream <= $numStreams; ++$stream) {
            $command = sprintf(
                self::STREAM_COMMAND_RUN_TEMPLATE,
                $this->job->getId(),
                $stream,
                $this->container->get('kernel')->getEnvironment() == 'prod' ? ' --env=prod --no-debug' : ''
            );

            $process = new Process($command);
            $process->start();
            $processes[] = $process;
        }

        return $processes;
    }

    /**
     * Wait for completion of all inventory processing streams.
     *
     * @param Process[] $processes
     * @param callable $onProgress
     *
     * @return void
     */
    protected function waitJobCompletion($processes, $onProgress)
    {
        while (true) {
            for ($proc = 0, $anyIsRuning = false; !$anyIsRuning && $proc < count($processes); ++$proc) {
                $anyIsRuning = $processes[$proc]->isRunning();
            }

            if (!$anyIsRuning) {
                break;
            }

            if (!$this->onJobProgressInternal($onProgress)) {
                $this->ensureJobCompleted();
                $this->stopAllJobProcesses($processes);
            }

            usleep(500000);
        }
    }

    protected function onJobCompletion($onCompletion)
    {
        $jobLogRepository = $this->container->get('app.repository.job_log');
        $resultRows  = $jobLogRepository->getJobLog($this->job->getId());
        call_user_func($onCompletion, $resultRows);
    }

    /**
     * Internal job processing callback to update quantity of processed items.
     *      *
     * @param callable $onProgress
     *
     * @return bool
     */
    protected function onJobProgressInternal($onProgress)
    {
        $numItemsLeft = $this->countUnprocessedItems();

        call_user_func($onProgress, $this->totalItemsQuantity, $this->totalItemsQuantity - $numItemsLeft);

        return $numItemsLeft > 0;
    }

    /**
     * Ensures job completion flag is true.
     */
    protected function ensureJobCompleted()
    {
        if (!$this->job->getComplete()) {
            $this->job->setComplete(true);
            $this->em->persist($this->job);
            $this->em->flush();
        }
    }

    /**
     * Stops all started processes.
     *
     * @param Process[] $processes
     *
     * @return void
     */
    protected function stopAllJobProcesses($processes)
    {
        array_walk($processes, function($proc) {
            /** @var Process $proc */
            $proc->stop();
        });
    }

}