<?php
/**
 * Created by PhpStorm.
 * User: vagrant
 * Date: 4/16/16
 * Time: 12:47 AM
 */

namespace AppBundle\Service;

use AppBundle\Entity\Inventory;
use AppBundle\Entity\Job;
use AppBundle\EventListener\OrderGeneratedEvent;
use AppBundle\Model\Order;
use AppBundle\Repository\InventoryRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class OrdersGenerator
{
    /** @var ContainerInterface */
    private $container;

    /** @var EntityManager */
    private $em;

    /** @var EventDispatcherInterface */
    private $dispatcher;

    /** @var InventoryRepository */
    private $inventoryRepository;

    /** @var int */
    private $maxInventoryUnitsPerLine;

    /** @var Inventory[] */
    private $inventories;


    const GENERATED_ORDERS_CHUNK_SIZE = 1000;


    /**
     * OrdersGenerator constructor.
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;

        $this->em = $this->container->get('doctrine')->getManager();
        $this->dispatcher = $this->container->get('event_dispatcher');
        $this->inventoryRepository = $this->container->get('app.repository.inventory');

        $this->maxInventoryUnitsPerLine = max($this->container->getParameter('max_inventory_units_per_line'), 1);

        $this->inventories = [];
    }

    /**
     *
     * Executes orders generation process, generating orders and publishing them via NSQ queue.
     *
     * @param Job $job
     * @param int $streamId
     */
    public function execute($job, $streamId)
    {
        mt_srand();

        $this->loadInventories();

        /** @var Order $order */
        for ($step = 1, $header = 1, $done = false; !$done; $step++) {
            $order = $this->generateOrder($job, $streamId, $header);

            if ($order->isValid()) {
                $this->publishOrder($order);
                $header++;
            }

            /**
             * Way to delay endless orders creation in case of no consumption of published messages.
             * TODO: ask about real scenario and needed action for the given (endless) case.
             */
            if ($step % self::GENERATED_ORDERS_CHUNK_SIZE == 0) {

                /* if received a signal...basically seems like never happens */
                if (is_array(time_nanosleep(5, 0))) {
                    $done = true;
                }
            } else {
                usleep(50000);
            }
        }
    }

    protected function loadInventories()
    {
        $this->inventories = $this->inventoryRepository->findAll();
    }

    /**
     * Generates new order for specified job, streamId and header.
     * Quantities for each inventory unit are generated randomly.
     *
     * @param Job $job
     * @param int $streamId
     * @param int $header
     *
     * @return Order
     */
    protected function generateOrder($job, $streamId, $header)
    {
        /** @var Order $order */
        $order = new Order($job->getId(), $streamId, $header);

        foreach ($this->inventories as &$inventory) {
            $quantity = mt_rand(0, $this->maxInventoryUnitsPerLine);
            $order->addLine($inventory->getName(), $quantity);
        }

        return $order;
    }

    /**
     * @param Order $order
     */
    protected function publishOrder($order)
    {
        $event = new OrderGeneratedEvent($order);
        $this->dispatcher->dispatch(OrderGeneratedEvent::EVENT_NAME, $event);
    }
}