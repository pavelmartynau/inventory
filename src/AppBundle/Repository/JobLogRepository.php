<?php

namespace AppBundle\Repository;

use AppBundle\Entity\JobLog;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;

/**
 * JobLogRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class JobLogRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * Adds new row into the job_log table.
     * Implemented for better performance.
     *
     * @param array $allocation
     *
     * @return bool
     * @throws \Doctrine\DBAL\DBALException
     */
    public function addNewRow($allocation)
    {
        $result = false;

        $sql = sprintf(
            'INSERT INTO `%s`(`job_id`, `stream`, `header`, `requested`, `allocated`, `backordered`)'.
            ' VALUES (:job_id, :stream, :header, :requested, :allocated, :backordered)',
            $this->getClassMetadata()->getTableName()
        );

        $statement = $this->getEntityManager()->getConnection()->prepare($sql);

        $statement->bindParam('job_id', $allocation['job'], \PDO::PARAM_INT);
        $statement->bindParam('stream', $allocation['stream'], \PDO::PARAM_INT);
        $statement->bindParam('header', $allocation['allocs']['header'], \PDO::PARAM_INT);

        foreach (['requested', 'allocated', 'backordered'] as $statsVar) {
            $statement->bindValue($statsVar, implode(',', $allocation['allocs']['res'][$statsVar]), \PDO::PARAM_STR);
        }

        if ($statement->execute()) {
            $result = ($statement->rowCount() == 1);
        }

        return $result;
    }

    /**
     * @param int $job_id
     * @param int $limit
     * @param int $page
     * @return array|Pagerfanta
     */
    public function getJobLog($job_id, $limit = 0, $page = 0)
    {
        $qb = $this->createQueryBuilder('jl');

        $qb->where('jl.job=:job_id')
            ->andWhere('jl.id <= :max_log_id')
            ->setParameter('job_id', (int)$job_id)
            ->setParameter(
                'max_log_id',
                (int)$this->getEntityManager()->createQuery(
                    'SELECT max(jl2.id) FROM AppBundle:JobLog jl2 '.
                    'WHERE jl2.job=:job_id AND jl2.allocated<>:zero_alloc'
                )
                    ->setParameter('zero_alloc', '0,0,0,0,0')
                    ->setParameter('job_id', (int)$job_id)
                    ->getSingleScalarResult()
            );

        if ($limit > 0) {
            $pagerAdapter = new DoctrineORMAdapter($qb);
            $pager = new Pagerfanta($pagerAdapter);
            $pager->setCurrentPage($page);
            $pager->setMaxPerPage($limit);
            return $pager;
        }
        else {
            return $qb->getQuery()->getArrayResult();
        }

    }

}
