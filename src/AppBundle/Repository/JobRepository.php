<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Job;
/**
 * JobsRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class JobRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * @return Job
     */
    public function createNewJob()
    {
        $job = new Job();
        $job->setComplete(false);
        $job->setCreatedAt(new \DateTime());
        $this->getEntityManager()->persist($job);
        $this->getEntityManager()->flush($job);

        return $job;
    }
}
