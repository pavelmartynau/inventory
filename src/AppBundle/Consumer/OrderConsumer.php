<?php
/**
 * Created by PhpStorm.
 * User: vagrant
 * Date: 4/16/16
 * Time: 4:27 AM
 */

namespace AppBundle\Consumer;

use AppBundle\Service\InventoryAllocator;
use AppBundle\Model\Order;
use Doctrine\ORM\EntityManagerInterface;
use Socloz\NsqBundle\Consumer\ConsumerInterface;


class OrderConsumer implements ConsumerInterface
{
    /**
     * @var InventoryAllocator
     */
    private $inventoryAllocator;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(
        InventoryAllocator $inventoryAllocator,
        EntityManagerInterface $em
    ) {
        $this->inventoryAllocator = $inventoryAllocator;
        $this->em = $em;
    }

    /**
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function consume($topic, $channel, $payload)
    {
        $this->ensureDatabaseConnected();

        $order = Order::createFromString($payload);

        if (!$order instanceof Order) {
            throw new \RuntimeException('Failed to unserialize Order object (payload='.$payload.'.');
        }

        $this->inventoryAllocator->allocate($order);
    }

    /**
     * Checks if database is connected and reconnects if needed.
     * Needed in case of long consumer work/wait and as a result
     * timeout and connection reset occurrence.
     */
    private function ensureDatabaseConnected()
    {
        if ($this->em->getConnection()->ping() === false) {
            $this->em->getConnection()->close();
            $this->em->getConnection()->connect();
        }
    }
}
