<?php

namespace AppBundle\Command;

use AppBundle\Entity\Job;
use AppBundle\Repository\JobRepository;
use AppBundle\Service\OrdersGenerator;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class InventoryStreamCommand extends ContainerAwareCommand
{
    /*
    * Constants
    */

    const JOB_ID_PARAM_NAME = 'job';
    const JOB_ID_PARAM_SHORTCUT = 'J';
    const STREAM_ID_PARAM_NAME = 'stream';
    const STREAM_ID_PARAM_SHORTCUT = 'S';


    /*
     * Fields
     */

    private $jobId;
    private $streamId;

    /** @var Job */
    private $job;

    protected function configure()
    {
        $this
            ->setName('inventory:stream')
            ->setDescription('Initiate inventory processing job')
            ->addOption(
                self::JOB_ID_PARAM_NAME,
                self::JOB_ID_PARAM_SHORTCUT,
                InputOption::VALUE_REQUIRED,
                'Inventory processing job id'
            )
            ->addOption(
                self::STREAM_ID_PARAM_NAME,
                self::STREAM_ID_PARAM_SHORTCUT,
                InputOption::VALUE_REQUIRED,
                'Inventory processing stream id'
            );
        ;
    }

    /**
     * Validates input parameters.
     *
     * @throws \InvalidArgumentException
     */
    protected function validateParams()
    {
        if (!$this->loadJobById($this->jobId)) {
            throw new \InvalidArgumentException(
                'Invalid job id ('.$this->jobId.') specified.'
            );
        }

        if ($this->streamId < 1) {
            throw new \InvalidArgumentException(
                'Invalid strems id ('.$this->streamId.') specified.'
            );
        }
    }

    /**
     * Checks if job id exists in the database.
     *
     * @param int $jobId
     *
     * @return bool
     */
    protected function loadJobById($jobId)
    {
        $this->job = null;

        if ($jobId > 0) {
            /** @var JobRepository $jobRepository */
            $jobRepository = $this->getContainer()->get('app.repository.job');

            $this->job = $jobRepository->find($jobId);
        }

        return !is_null($this->job);
    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->jobId = $input->getOption(self::JOB_ID_PARAM_NAME);
        $this->streamId = $input->getOption(self::STREAM_ID_PARAM_NAME);
        $this->validateParams();

        $startTime = time();
        $output->writeln('Stream started at '.date('H:i:s m/d/Y', $startTime));

        $this->generateOrders();

        $endTime = time();

        $output->writeln(
            "\nStream ended at ".date(
                'H:i:s m/d/Y',
                $endTime
            ).' Total stream work time: '.($endTime - $startTime).' s.'
        );
    }
    
    protected function generateOrders()
    {
        /** @var OrdersGenerator $ordersGenerator */
        $ordersGenerator = $this->getContainer()->get('app.orders.generator');
        $ordersGenerator->execute($this->job, $this->streamId);
    }
}
