<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class InventoryProcessCommand extends ContainerAwareCommand
{
    /*
     * Constants
     */

    const NUM_STREAMS_PARAM_NAME = 'num-streams';
    const NUM_STREAMS_PARAM_SHORTCUT = 'N';

    const DEFAULT_NUM_STREAMS = 2;

    /*
     * Fields
     */

    private $numStreams = self::DEFAULT_NUM_STREAMS;

    /** @var OutputInterface */
    private $output;

    protected function configure()
    {
        $this
            ->setName('inventory:process')
            ->setDescription('Initiate inventory processing job')
            ->addOption(
                self::NUM_STREAMS_PARAM_NAME,
                self::NUM_STREAMS_PARAM_SHORTCUT,
                InputOption::VALUE_OPTIONAL,
                'Number of streams, used for the job',
                self::DEFAULT_NUM_STREAMS
            );
    }


    /**
     * @throws \InvalidArgumentException
     */
    protected function validateParams()
    {
        if ($this->numStreams < 1 && $this->numStreams > 20) {
            throw new \InvalidArgumentException(
                'Valid number of strems is between 1 and 20. '.$this->numStreams.' requested.'
            );
        }
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->numStreams = $input->getOption(self::NUM_STREAMS_PARAM_NAME);

        $this->validateParams();

        $startTime = time();
        $output->writeln('Processing started at '.date('H:i:s m/d/Y', $startTime));

        $this->output = $output;
        $this->initiateInventoryProcessingJob();

        $endTime = time();

        $output->writeln(
            "\nProcessing ended at ".date(
                'H:i:s m/d/Y',
                $endTime
            ).' Total processing time: '.($endTime - $startTime).' s.'
        );
    }

    /**
     * @throws \RuntimeException
     * @return bool
     */
    protected function initiateInventoryProcessingJob()
    {
        $job = $this->getContainer()->get('app.inventory.processing.job');
        $job->execute($this->numStreams, [$this, 'displayProgress'], [$this, 'dumpResult']);
    }

    public function displayProgress($totalItems, $itemsProcessed)
    {
        static $prevPercentProcessed = -1;

        if ($totalItems > 0) {
            $percentProcessed = round($itemsProcessed / $totalItems * 100, 2);
            if ($percentProcessed != $prevPercentProcessed) {
                $tmpl = sprintf("\r%%0%dd of %%d processed (%% 1.2f%%%%)", strlen((string)$totalItems));

                $this->output->write(sprintf($tmpl, $itemsProcessed, $totalItems, $percentProcessed));

                $prevPercentProcessed = $percentProcessed;
            }
        }
    }

    public function dumpResult($resultRows)
    {
        echo "\n\n";
        array_walk($resultRows, function ($row) {
            printf(
                "% 6d(%02d) :\t%s::%s::%s".PHP_EOL,
                $row['header'],
                $row['stream'],
                $row['requested'],
                $row['allocated'],
                $row['backordered']
            );
        });
    }

}