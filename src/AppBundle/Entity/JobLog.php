<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * JobLog
 *
 * @ORM\Table(name="job_log")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\JobLogRepository")
 */
class JobLog
{
    public function __construct(Job $job)
    {
        $this->job = $job;
    }

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Job
     *
     * @ORM\ManyToOne(targetEntity="Job", inversedBy="job", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="job_id", referencedColumnName="id", nullable=false)
     */
    private $job;

    /**
     * @var int
     *
     * @ORM\Column(name="stream", type="integer", options={"unsigned"=true})
     */
    private $stream;

    /**
     * @var int
     *
     * @ORM\Column(name="header", type="integer", options={"unsigned"=true})
     */
    private $header;

    /**
     * @var string
     *
     * @ORM\Column(name="requested", type="string", length=255)
     */
    private $requested;

    /**
     * @var string
     *
     * @ORM\Column(name="allocated", type="string", length=255)
     */
    private $allocated;

    /**
     * @var string
     *
     * @ORM\Column(name="backordered", type="string", length=255)
     */
    private $backordered;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get job
     *
     * @return Job
     */
    public function getJob()
    {
        return $this->job;
    }


    /**
     * Set stream
     *
     * @param integer $stream
     *
     * @return JobLog
     */
    public function setStream($stream)
    {
        $this->stream = $stream;

        return $this;
    }

    /**
     * Get stream
     *
     * @return int
     */
    public function getStream()
    {
        return $this->stream;
    }

    /**
     * Set header
     *
     * @param integer $header
     *
     * @return JobLog
     */
    public function setHeader($header)
    {
        $this->header = $header;

        return $this;
    }

    /**
     * Get header
     *
     * @return int
     */
    public function getHeader()
    {
        return $this->header;
    }

    /**
     * Set requested
     *
     * @param string $requested
     *
     * @return JobLog
     */
    public function setRequested($requested)
    {
        $this->requested = $requested;

        return $this;
    }

    /**
     * Get requested
     *
     * @return string
     */
    public function getRequested()
    {
        return $this->requested;
    }

    /**
     * Set allocated
     *
     * @param string $allocated
     *
     * @return JobLog
     */
    public function setAllocated($allocated)
    {
        $this->allocated = $allocated;

        return $this;
    }

    /**
     * Get allocated
     *
     * @return string
     */
    public function getAllocated()
    {
        return $this->allocated;
    }

    /**
     * Set backordered
     *
     * @param string $backordered
     *
     * @return JobLog
     */
    public function setBackordered($backordered)
    {
        $this->backordered = $backordered;

        return $this;
    }

    /**
     * Get backordered
     *
     * @return string
     */
    public function getBackordered()
    {
        return $this->backordered;
    }
}

