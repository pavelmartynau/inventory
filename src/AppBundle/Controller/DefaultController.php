<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Inventory;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $inventoryRepository = $this->getDoctrine()->getRepository('AppBundle:Inventory');

        /** @var Inventory[] $allInventory */
        $allInventory = $inventoryRepository->findAll();

        return $this->render('AppBundle::index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..'),
            'inventoryItems' => $allInventory
        ]);
    }
}
