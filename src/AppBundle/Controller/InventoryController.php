<?php
/**
 * Created by PhpStorm.
 * User: vagrant
 * Date: 4/19/16
 * Time: 5:27 AM
 */

namespace AppBundle\Controller;

use AppBundle\Repository\InventoryRepository;
use Hateoas\Configuration\Route;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;


class InventoryController extends Controller
{
    /**
     * @Rest\Put(name="update_inventories", path="/api/inventories", defaults={"_format" = "json"})
     */
    public function inventoriesAddAction(Request $request)
    {
        $error = 0;
        
        /** @var InventoryRepository $inventoryRepository */
        $inventoryRepository = $this->getDoctrine()->getManager()
            ->getRepository('AppBundle:Inventory');

        $inventoryValues = json_decode($request->getContent(), true);
        try {
            $inventoryRepository->addUnits($inventoryValues);
        } catch (Exception $ex) {
            $error = $ex->getMessage();
        }

        $statusCode = $error === 0 ? 200 : 400; //always return 400 bad request in case of error for now
        $response = new JsonResponse();
        $response->setData(array(
            'status' => $statusCode,
            'error' => $error !== 0 ? $error : '',
        ));
        $response->setStatusCode($statusCode);

        return $response;
    }
}