<?php
/**
 * Created by PhpStorm.
 * User: vagrant
 * Date: 4/19/16
 * Time: 4:21 PM
 */

namespace AppBundle\Controller;

use AppBundle\Repository\JobRepository;
use Hateoas\Configuration\Route;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;


class JobController extends Controller
{
    /**
     * @Rest\Post(name="new_job", path="/api/jobs", defaults={"_format" = "json"})
     */
    public function jobsAddAction(Request $request)
    {
        $error = 0;
        $new_job_id = 0;

        try {
            /** @var JobRepository $jobRepository */
            $jobRepository = $this->container->get('app.repository.job');
            $job = $jobRepository->createNewJob();
            $new_job_id = $job->getId();
        } catch (Exception $ex) {
            $error = $ex->getMessage();
        }

        $statusCode = $error === 0 ? 201 : 403; //always return 403 forbidden in case of error for now
        $response = new JsonResponse();
        $response->setData(array(
            'job' => $error === 0 ? $new_job_id : 0,
            'status' => $statusCode,
            'error' => $error !== 0 ? $error : '',
        ));
        $response->setStatusCode($statusCode);

        return $response;
    }
}