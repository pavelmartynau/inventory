<?php
/**
 * Created by PhpStorm.
 * User: vagrant
 * Date: 4/17/16
 * Time: 8:44 PM
 */

namespace AppBundle\Controller;


use AppBundle\Repository\JobLogRepository;
use Hateoas\Representation\Factory\PagerfantaFactory;
use Hateoas\Configuration\Route;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;


class JobLogController extends Controller
{
    /**
     * @Rest\Get(name="job_log_list", path="/api/joblog/{job_id}", defaults={"_format" = "json"})
     * @Rest\View()
     */
    public function getJobLogAction($job_id, Request $request)
    {
        $limit = $request->query->getInt('limit', 10);
        $page = $request->query->getInt('page', 1);

        /** @var JobLogRepository $jobLogRepository */
        $jobLogRepository = $this->getDoctrine()->getManager()
            ->getRepository('AppBundle:JobLog');

        $jobLogPager = $jobLogRepository->getJobLog($job_id, $limit, $page);

        $pagerFactory = new PagerfantaFactory();

        return $pagerFactory->createRepresentation(
            $jobLogPager,
            new Route('job_log_list', array(
                'job_id' => $job_id,
                'limit' => $limit,
                'page' => $page
            ))
        );

    }

}