Inventory allocation
=========

## Task definition ##
The object of the exercise is to show off your coding style and skill.

Initial conditions:
  Initially, the system contains inventory of

```
#!

  A x 150
  B x 150
  C x 100
  D x 100
  E x 200
```


  Initially, the system contains no orders

Data source:
  There should be a data source capable of generating one or more streams of orders.
  An order consists of a unique identifier (per stream) we will call the "header", and a demand for between zero and five units each of A,B,C,D, and E, except that there must be at least one unit demanded.
  A valid order (in whatever format you choose): {"Header": 1, "Lines": {"Product": "A", "Quantity": "1"},{"Product": "C", "Quantity": "4"}}
  An invalid order: {"Header": 1, "Lines": {"Product": "B", "Quantity": "0"}}
  Another invalid order: {"Header": 1, "Lines": {"Product": "D", "Quantity": "6"}}

Inventory allocator:
  There should be an inventory allocator which allocates inventory to the inbound data according to the following rules:
  1) Inbound orders to the allocator should be individually identifyable (ie two streams may generate orders with an identical header, but these orders should be identifyable from their streams)
  2) Inventory should be allocated on a first come, first served basis; once allocated, inventory is not available to any other order.
  3) Inventory should never drop below 0.
  4) If a line cannot be satisfied, it should not be allocated.  Rather, it should be  backordered (but other lines on the same order may still be satisfied).
  5) When all inventory is zero, the system should halt and produce output listing, in the order received by the system, the header of each order, the quantity on each line, the quantity allocated to each line, and the quantity backordered for each line.
  For instance:
  If the initial conditions are:

```
#!

  A x 2
  B x 3
  C x 1
  D x 0
  E x 0
```


  And the input is:

```
#!

  {"Header": 1, "Lines": {"Product": "A", "Quantity": "1"}{"Product": "C", "Quantity": "1"}}
  {"Header": 2, "Lines": {"Product": "E", "Quantity": "5"}}
  {"Header": 3, "Lines": {"Product": "D", "Quantity": "4"}}
  {"Header": 4, "Lines": {"Product": "A", "Quantity": "1"}{"Product": "C", "Quantity": "1"}}
  {"Header": 5, "Lines": {"Product": "B", "Quantity": "3"}}
  {"Header": 6, "Lines": {"Product": "D", "Quantity": "4"}}
```

 
  The output should be (in whatever format you choose):

```
#!

  1: 1,0,1,0,0::1,0,1,0,0::0,0,0,0,0
  2: 0,0,0,0,5::0,0,0,0,0::0,0,0,0,5
  3: 0,0,0,4,0::0,0,0,0,0::0,0,0,4,0
  4: 1,0,1,0,0::1,0,0,0,0::0,0,1,0,0
  5: 0,3,0,0,0::0,3,0,0,0::0,0,0,0,0
```


## Implementation details: ##

Following are tools and languages, used for the task implementation: PHP 5.5, Symfony 2.8, MariaDB, NSQ.

### Installation: ###

Current version was tested on Centos 7.1 with php-fpm. Additionally required:

* NSQ  - see http://nsq.io/deployment/installing.html
* Composer - see https://getcomposer.org/download/


### Steps of installation: ###

1. Download the project: git clone https://bitbucket.org/pavelmartynau/inventory.git
2. Run composer install
3. Create mysql database for the application (should match the name from the parameters.yml)
4. Run app/console doctrine:migrations:migrate to create necessary mysql structure
5. Run app/console assetic:dump

So, currently the application works via Symfony console commands and NSQ consumers. UI is partly implemented (Angular+Bootstrap), but due to issues (like possible long processing of data + default timeout of NSQ consumers of 1 minute) I had to stop with the UI implementation to revisit it later with better concept of running requested jobs from browser application. Right now from browser you can only increase inventory of all types (from A to E). Initially inventory table is filled with requested values:

```
#!

  A x 150
  B x 150
  C x 100
  D x 100
  E x 200
```


In order to run the app, first please start NSQ daemon. It is also needed to pre-create special topic that the application consumers will listen to (topic name is 'orders'). You may use following shell script:


```
#!

app/nsq-create-topic.sh orders
```

Now you can start one or many consumers using following command:


```
#!

app/console socloz:nsq:topic:consume orders &
```

This topic is used by the application consumer class to receive pre-created orders and allocate inventory from order lines. In the task definition it's not said that there should be many consumers, but the app works with one consumer as well as with many. It is recommended to balance number of consumers with number of streams used to create orders (see below). For example if you run 20 streams then the optimal number of consumers is 3-5. Otherwise the queue of published orders will grow quickly and after the process complete (no more inventory left) the queue will still contain a lot of unprocessed orders. This will not affect resulted allocations, but just will make process of orders handling longer.

In order to publish orders in streams (simultaneously) there were created two Symfony console commands:


```
#!

1. inventory:process
2. inventory:stream

```

The first one - inventory:process - is used for initiation of orders generating streams. It may get one optional parameter:

```
#!

-N, --num-streams[=NUM-STREAMS]  Number of streams, used for the job [default: 2]
```

So, the following command can be used to start 5 streams of orders generation process:


```
#!

app/console inventory:process --num-streams=5

```

The above will create new job object (session) and start 5 instances of another implemented Symfony command: "inventory:stream". During orders generation the main process will also show current processing statistics and will finish as soon as all inventories will be allocated (ending inventory:stream processes). Also, according to the task definition, after all inventory is allocated the resulted allocations list will be dumped. The format is very similar to what was shown in the task description, except one addition - I also show stream # as it's easy to distinguish different streams with the same header. Following is an example of the output (not full output, but just a part of it; 3 streams used):


```
#!

    36(03) :	2,5,1,2,4::2,0,1,0,0::0,5,0,2,4
    36(02) :	2,1,1,1,1::2,0,1,1,0::0,1,0,0,1
    36(01) :	0,0,1,4,1::0,0,1,0,0::0,0,0,4,1
    37(03) :	4,1,5,4,1::4,0,5,0,0::0,1,0,4,1
    37(02) :	3,4,3,0,3::3,0,3,0,0::0,4,0,0,3
    37(01) :	1,1,3,3,4::1,0,3,0,0::0,1,0,3,4
    38(03) :	1,3,5,0,3::1,0,5,0,0::0,3,0,0,3
    38(02) :	3,0,1,4,1::3,0,1,0,0::0,0,0,4,1
    38(01) :	3,2,0,4,5::3,0,0,0,0::0,2,0,4,5
    39(03) :	5,4,4,3,4::5,0,4,0,0::0,4,0,3,4
    39(02) :	1,3,3,3,5::1,0,0,0,0::0,3,3,3,5
    39(01) :	3,3,1,5,0::3,0,1,0,0::0,3,0,5,0

```