<?php

namespace Application\Migrations;

use AppBundle\Entity\Inventory;
use AppBundle\Repository\InventoryRepository;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;


class Version20160415193512 extends AbstractMigration implements ContainerAwareInterface
{
    private static $defaultInventoryData = [
        'A' => 150,
        'B' => 150,
        'C' => 100,
        'D' => 100,
        'E' => 200,
    ];

    /**
     * @var Container $container
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * @return EntityManager
     * @throws \Exception
     */
    protected function getEntityManager()
    {
        return $this->container->get('doctrine')->getManager();
    }

    /**
     * @return InventoryRepository
     * @throws \Exception
     */
    protected function getInventoryRepository()
    {
        return $this->container->get('app.repository.inventory');
    }

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $em = $this->getEntityManager();

        array_walk(
            self::$defaultInventoryData,
            function ($invQuantity, $invName) use ($em) {
                $inventory = new Inventory;
                $inventory->setName($invName);
                $inventory->setQuantity($invQuantity);
                $em->persist($inventory);
            }
        );

        $em->flush();

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $inventoryRepository = $this->getInventoryRepository();

        $inventoryNames = array_keys(self::$defaultInventoryData);
        array_walk(
            $inventoryNames,
            function ($invName) use ($inventoryRepository) {
                $inventoryRepository->deleteByName($invName);
            }
        );

        $this->getEntityManager()->flush();
    }
}
